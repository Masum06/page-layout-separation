import numpy as np
import cv2

inputImageName = 'inputImage.png'

def findCropPositions( img ):
    border = 5
    height, width = img.shape
    xlist = []
    ylist = []
    for y in range( width ):
        for x in range( height ):
            if img[x][y] < 10:
                xlist.append( x )
                ylist.append( y )
    xleft = min( xlist )
    xright = max( xlist )
    ytop = min( ylist )
    ybot = max( ylist )
    
    borderAdjustedXleft = max( xleft-border, 0 )
    borderAdjstedXright = min( xright+border, height )
    borderAdjustedYtop = max( ytop-border, 0 )
    borderAdjustedYbot = min( ybot+border, width )

    cropPositions = {
    "xleft" : borderAdjustedXleft,
    "xright" : borderAdjstedXright,
    "ytop" : borderAdjustedYtop,
    "ybot" : borderAdjustedYbot
    }
    print( cropPositions )
    return cropPositions

def removeNonText( img, cropPositions ):
	xleft = cropPositions["xleft"]
	xright = cropPositions["xright"]
	ytop = cropPositions["ytop"]
	ybot = cropPositions["ybot"]

	blankImage = np.copy( img )

	height, width = img.shape
	for w in range( width ):
		for h in range( height ):
			if w>=xleft and w<=xright and h<=ybot and h>=ytop:
				blankImage[w][h] = 255
	return blankImage

def invert( img ):
	invertedImage = cv2.bitwise_not( img )
	return invertedImage




inputImage = cv2.imread(inputImageName, 0 )
thresholdValue = 150
thresholdedWhiteValue = 255

print( inputImage.shape )
print( type( inputImage ) )

ret3,binarizedImage = cv2.threshold( inputImage,thresholdValue,thresholdedWhiteValue,\
	cv2.THRESH_BINARY )

blurredImage = cv2.medianBlur( binarizedImage,41 )
blurredImage = cv2.blur( blurredImage,(5,5 ) )
ret3,blurredImage = cv2.threshold( blurredImage,0,thresholdedWhiteValue,\
	cv2.THRESH_BINARY )

cropPositions = findCropPositions( blurredImage )

textImage = removeNonText( binarizedImage, cropPositions )
nonTextImage = invert( invert( binarizedImage ) - invert( textImage ) )

cv2.imshow('adaptiveThreshold', nonTextImage )
cv2.waitKey( 0 )
cv2.destroyAllWindows( )

cv2.imwrite('output/binarizedImage.png', binarizedImage )
cv2.imwrite('output/textImage.png', textImage )
cv2.imwrite('output/nonTextImage.png', nonTextImage )