# Finds the black pixels in white background and crops it
import numpy as np
import cv2
from glob import glob
import os
import sys

# This function CROPS
def zoom(img):
    height, width = img.shape
    xlist = []
    ylist = []
    for y in range(width):
        for x in range(height):
            if img[x][y] < 10:
                xlist.append(x)
                ylist.append(y)
    xleft = min(xlist)
    xright = max(xlist)
    ytop = min(ylist)
    ybot = max(ylist)
    cropped = img[ max(xleft-10, 0): min(xright+10, height), max(ytop-10, 0): min(ybot+10, width)]
    return cropped

""""def zoom(img):
	height, width, channel = img.shape[:3]
	xlist = []
	ylist = []
	for y in range(width):
	    for x in range(height):
	        if (img.item(x,y,1) == 0 and img.item(x,y,2) == 0 and img.item(x,y,0) == 0)  :
	            xlist.append(x)
	            ylist.append(y)
	xleft = min(xlist)
	xright = max(xlist)
	ytop = min(ylist)
	ybot = max(ylist)
	cropped = img[ xleft-10: xright+10, ytop-10: ybot+10]
	return cropped"""

img = cv2.imread('blurred.png',0)
img = zoom(img)
cv2.imwrite("blurred_cropped.png", img)
cv2.waitKey(0)
cv2.destroyAllWindows()

# start = 0
# end = start+10
# source_folder = start
# for source_folder in range(start, end):
# 	dest_folder = source_folder-start
# 	im_files = glob('./data/{}/*.png'.format(source_folder))
# 	print(im_files[0])
# 	#sys.exit(0)
# 	for j in range(len(im_files)):
# 		img = cv2.imread(im_files[j])
# 		img = zoom(img)
# 		dest = "datacrop/"+str(dest_folder)
# 		os.makedirs(dest, exist_ok=True)
# 		cv2.imwrite( "datacrop/{}/{}.png".format(dest_folder, j), img) #im_files[0] +
